import pygame
import random
import time

# Pygameの初期化
pygame.init()

# ゲーム画面の設定
screen_width = 800
screen_height = 600
img_bg_original = pygame.image.load("asset/bgimage.png")
img_bg = pygame.transform.scale(img_bg_original,(800,600))
screen = pygame.display.set_mode((screen_width, screen_height))
time_sta = 0
time_end = 0
# ゲームのタイトルを設定
pygame.display.set_caption("Get A Unit")

# 色の定義
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)

# プレイヤーの設定
player_size = 50
player_pos = [screen_width // 2, screen_height - player_size]
player_speed = 10
player_level = 0
player_health = 5

# 敵の設定
enemy_size = 50
boss_size_x = 193
boss_size_y = 257

enemies1 = []
enemies2 = []
enemies3 = []
health_item = []
level_up_item = []
bomb = []

boss_flag = 0
boss = []
boss_hp = 100

item_flag = 0

bg_y = 0

stage = 0

enemy2_flag = 0
enemy3_flag = 0

stage1_num = [20,0,0]
stage2_num = [10,20,0]
stage3_num = [10,10,30]

# 敵の速度
enemy_speed = 1
# 敵の速度増加のためのスコアの閾値
score_for_speed_increase = 3000

# ミサイルの設定
missile_size = [10, 10]
missiles = []

# テキストの設定
title_font = pygame.font.Font(None, 100)
title_text = title_font.render('Press Enter to Start', True, WHITE)
start_font = pygame.font.Font(None, 50)
start_text = start_font.render('Press Enter to Start', True, WHITE)
game_over_font = pygame.font.Font(None, 75)
game_over_text = game_over_font.render('GAME OVER', True, WHITE)
game_over_restart_font = pygame.font.Font(None, 36)
game_over_restart_text = start_font.render('Press Enter to Restart or ESC to Quit', True, WHITE)

# スコアの設定
score = 0
score_font = pygame.font.Font(None, 36)

# プレイヤーと敵の画像を読み込む
player_image_original = pygame.image.load('asset/player.png')
enemy1_image_original = pygame.image.load('asset/enemy1.png')
enemy2_image_original = pygame.image.load('asset/enemy2.png')
enemy3_image_original = pygame.image.load('asset/enemy3.png')
boss_image_original = pygame.image.load('asset/boss.png')
heart_image_original = pygame.image.load('asset/heart.png')
level_up_image_original = pygame.image.load('asset/mybullet.png')
hp_image_original = pygame.image.load('asset/heart.png')
bomb_image_original = pygame.image.load('asset/bomb.png')


# 画像のサイズを50ピクセルに設定
player_image = pygame.transform.scale(player_image_original, (player_size, player_size))
enemy1_image = pygame.transform.scale(enemy1_image_original, (enemy_size, enemy_size))
enemy2_image = pygame.transform.scale(enemy2_image_original, (enemy_size, enemy_size))
enemy3_image = pygame.transform.scale(enemy3_image_original, (enemy_size, enemy_size))
boss_image = pygame.transform.scale(boss_image_original,(boss_size_x,boss_size_y))
heart_image = pygame.transform.scale(heart_image_original,(enemy_size,enemy_size))
level_up_image =  pygame.transform.scale(level_up_image_original,(enemy_size,enemy_size))
hp_image = pygame.transform.scale(hp_image_original,(25,25))
bomb_image = pygame.transform.scale(bomb_image_original, (enemy_size, enemy_size))

# ゲームの基本設定
clock = pygame.time.Clock()
game_over = False

# スコアの描画
def draw_score():
    score_text = score_font.render('Score: ' + str(score), True, WHITE)
    screen.blit(score_text, (10, 10))

# def draw_stage():
#     stage_text = score_font.render('Stage: ' + str(stage), True, WHITE)
#     screen.blit(stage_text, (10, 35))

def draw_health():
    stage_text = score_font.render('HP: ', True, WHITE)
    screen.blit(stage_text, (10, 35))
    for i in range(player_health):
        screen.blit(hp_image, (i*30 + 60, 35))
    


# 敵の移動
def drop_enemies(enemies,num):
    global stage,stage1_num,stage2_num,stage3_num,boss_flag
    delay = random.randint(1, 50)
    if len(enemies) < 10 and random.randint(1, delay) == 1:
        x_pos = random.randint(0, screen_width-enemy_size)
        y_pos = 0
        if stage == 0:
            if num == 1 and stage1_num[0] > 0:
                enemies.append([x_pos, y_pos])
            elif num == 2 and stage1_num[1] > 0:
                enemies.append([x_pos, y_pos])
            elif num == 3 and stage1_num[2] > 0:
                enemies.append([x_pos, y_pos])
        elif stage == 1:
            if num == 1 and stage2_num[0] > 0:
                enemies.append([x_pos, y_pos])
            elif num == 2 and stage2_num[1] > 0:
                enemies.append([x_pos, y_pos])
            elif num == 3 and stage2_num[2] > 0:
                enemies.append([x_pos, y_pos])

        elif stage == 2:
            if num == 1 and stage3_num[0] > 0:
                enemies.append([x_pos, y_pos])
            elif num == 2 and stage3_num[1] > 0:
                enemies.append([x_pos, y_pos])
            elif num == 3 and stage3_num[2] > 0:
                enemies.append([x_pos, y_pos])

        elif stage == 3:
            if boss_flag == 0:
                boss_flag = 1
                enemies.append([(screen_width - boss_size_x)/2, 30])
                
                #enemies.append([0, 0])
            else :
                pass

def drop_items(items,x_pos,y_pos,num):
    global stage,stage1_num,stage2_num,stage3_num,boss_flag
    if num == 1:
        delay = random.randint(1, 100)
        if len(items) < 10 and random.randint(1, delay) == 1:
            items.append([x_pos, y_pos])
    elif num == 2 :
        items.append([x_pos, y_pos])
    else : 
        delay = random.randint(1, 100)
        if len(items) < 100 and random.randint(1, delay) == 1:
            items.append([x_pos, y_pos + 257])
# 敵の描画
            
def draw_item(items,num):
    if num == 1:
        for item_pos in items:
            screen.blit(heart_image, (item_pos[0], item_pos[1]))
    elif num == 2:
         for item_pos in items:
            screen.blit(level_up_image, (item_pos[0], item_pos[1]))
    else :
        for item_pos in items:
             screen.blit(bomb_image, (item_pos[0], item_pos[1]))

def draw_enemies(enemies,num):
    if num == 1: 
        for enemy_pos in enemies:
            screen.blit(enemy1_image, (enemy_pos[0], enemy_pos[1]))
    if num == 2:
        for enemy_pos in enemies:
            screen.blit(enemy2_image, (enemy_pos[0], enemy_pos[1]))
    if num == 3:
         for enemy_pos in enemies:
            screen.blit(enemy3_image, (enemy_pos[0], enemy_pos[1]))
    if num == 4 :
        for enemy_pos in enemies:
            screen.blit(boss_image, (enemy_pos[0], enemy_pos[1]))

# プレイヤーの描画
def draw_player():
    global bg_y 
    screen.blit(player_image, (player_pos[0], player_pos[1]))

# 的位置の更新
def update_enemy_positions(enemies, player_pos,num):
    global enemy2_flag,enemy3_flag,player_health,bomb
    if num ==2:
        for idx, enemy_pos in enumerate(enemies):
            if enemy_pos[0] <= 0:
                enemy2_flag = 0
            elif enemy_pos[0] > screen_width - enemy_size:
                enemy2_flag = 1

            if enemy_pos[1] >= 0 and enemy_pos[1] < screen_height:
                enemy_pos[1] += 2
                if enemy2_flag == 0:
                    enemy_pos[0] += 2
                elif enemy2_flag == 1:
                    enemy_pos[0] -= 2
                # プレイヤーと敵の衝突判定
                if player_pos[1] < enemy_pos[1] + enemy_size and \
                player_pos[0] < enemy_pos[0] + enemy_size and \
                player_pos[0] + player_size > enemy_pos[0]:
                    player_health -= 1
                    enemies.pop(idx)
            else:
                enemies.pop(idx)
    if num ==3:
        for idx, enemy_pos in enumerate(enemies):

            if enemy_pos[0] <= player_pos[0]:
                enemy_pos[0] += 4
            elif enemy_pos[0] >= player_pos[0]:
                enemy_pos[0] -= 4

            if enemy_pos[1] >= 0 and enemy_pos[1] < screen_height:
                enemy_pos[1] += 4

                # プレイヤーと敵の衝突判定
                if player_pos[1] < enemy_pos[1] + enemy_size and \
                player_pos[0] < enemy_pos[0] + enemy_size and \
                player_pos[0] + player_size > enemy_pos[0]:
                    player_health -= 1
                    enemies.pop(idx)
            else:
                enemies.pop(idx)
    if num ==4:
        for idx, enemy_pos in enumerate(enemies):
            drop_items(bomb,enemy_pos[0],enemy_pos[1],3)
            if enemy_pos[0] < 0:
                enemy3_flag = 0
            elif enemy_pos[0] > screen_width - boss_size_x:
                enemy3_flag = 1

            if enemy_pos[1] >= 0 and enemy_pos[1] < screen_height:
                if enemy3_flag == 0:
                    enemy_pos[0] += 5
                elif enemy3_flag == 1:
                    enemy_pos[0] -= 5
                # プレイヤーと敵の衝突判定
                if player_pos[1] < enemy_pos[1] + boss_size_y and \
                player_pos[0] < enemy_pos[0] + boss_size_x and \
                player_pos[0] + player_size > enemy_pos[0]:
                    player_health -= 1
                    enemies.pop(idx)
            else:
                enemies.pop(idx)

   
    else :
        for idx, enemy_pos in enumerate(enemies):
            if enemy_pos[1] >= 0 and enemy_pos[1] < screen_height:
                enemy_pos[1] += enemy_speed
                # プレイヤーと敵の衝突判定
                if player_pos[1] < enemy_pos[1] + enemy_size and \
                player_pos[0] < enemy_pos[0] + enemy_size and \
                player_pos[0] + player_size > enemy_pos[0]:
                    player_health -= 1
                    enemies.pop(idx)
            else:
                enemies.pop(idx)


def update_item_positions(items, player_pos,num):
    global player_health,player_level,bomb
    
    for idx, item_pos in enumerate(items):
        if item_pos[1] > 0 and item_pos[1] < screen_height:
            item_pos[1] += 2
            if num == 3:
                item_pos[1] += 3
            # プレイヤーと敵の衝突判定
            if player_pos[1] < item_pos[1] + enemy_size and \
            player_pos[0] < item_pos[0] + enemy_size and \
            player_pos[0] + player_size > item_pos[0]:
                items.pop(idx)
                #items.remove(item_pos)
                if num == 1:
                    player_health += 1
                elif num == 2:
                    player_level += 1
                else:
                    player_health -=1
    
        else:
            items.pop(idx)


# ミサイル発射
def fire_missile(x, y):
    missiles.append([x, y])

# ミサイルの移動
def move_missiles(missiles):
    global player_level
    if player_level == 0 :
        for missile in missiles:
            missile[1] -= 10
            if missile[1] < 0:
                missiles.remove(missile)
    elif player_level == 1:
        for missile in missiles:
            missile[1] -= 12
            if missile[1] < 0:
                missiles.remove(missile)
    elif player_level == 2:
        for idx, missile in enumerate(missiles):
            idx = random.randint(0,2)
            if idx == 0:
                missile[1] -= 15
                missile[0] += 5
            if idx == 1:
                missile[1] -= 15
                missile[0] -= 5
            if idx == 2:
                missile[1] -= 15

            if missile[1] < 0:
                missiles.remove(missile)
    else :
        for missile in missiles:
            missile[1] -= 20
            if missile[1] < 0:
                missiles.remove(missile)
        pass


# ミサイルの描画
def draw_missiles(missiles):
    global player_level
    if player_level == 0:
        for missile in missiles:
            pygame.draw.rect(screen, GREEN, (missile[0], missile[1], missile_size[0], missile_size[1]))
    elif player_level == 1:
        for missile in missiles:
            pygame.draw.rect(screen, GREEN, (missile[0], missile[1], missile_size[0], missile_size[1]))
    elif player_level == 2:
        for missile in missiles:
            pygame.draw.rect(screen, GREEN, (missile[0], missile[1], missile_size[0], missile_size[1]))
    elif player_level == 3:
        for missile in missiles:
            pygame.draw.rect(screen, GREEN, (missile[0], missile[1], missile_size[0], missile_size[1]))

# 衝突のチェック
def collision_check(enemies, missiles,num):
    global score, enemy_speed, score_for_speed_increase,stage,stage1_num,stage2_num,stage3_num,stage,boss_hp,health_item,level_up_item,bomb
    for enemy in enemies[:]:
        for missile in missiles[:]:
            # 敵とミサイルの衝突判定
            if (missile[0] >= enemy[0] and missile[0] < enemy[0] + enemy_size) and \
               (missile[1] >= enemy[1] and missile[1] < enemy[1] + enemy_size):
                try:
                    if stage == 0:
                        if num == 1:
                            enemies.remove(enemy)
                            missiles.remove(missile)
                            drop_items(health_item,enemy[0],enemy[1],1)
                            #enemies1_num[stage] -= 1
                            stage1_num[0] -= 1
                            score += 100

                            if stage1_num[0] == 0 and stage1_num[1] == 0 and stage1_num[2] == 0:
                                drop_items(level_up_item,enemy[0],enemy[1],2)
                    
                    if stage == 1:
                        if num == 1:
                            enemies.remove(enemy)
                            missiles.remove(missile)
                            drop_items(health_item,enemy[0],enemy[1],1)
                            #enemies1_num[stage] -= 1
                            stage2_num[0] -= 1
                            score += 100
                        elif num == 2:
                            enemies.remove(enemy)
                            missiles.remove(missile)
                            drop_items(health_item,enemy[0],enemy[1],1)
                            #enemies1_num[stage] -= 1
                            stage2_num[1] -= 1
                            score += 200
                        
                        if stage2_num[1] == 0 and stage2_num[2] == 0:
                                drop_items(level_up_item,enemy[0],enemy[1],2)
                    if stage == 2:
                        if num == 1:
                            enemies.remove(enemy)
                            missiles.remove(missile)
                            drop_items(health_item,enemy[0],enemy[1],1)
                            #enemies1_num[stage] -= 1
                            stage3_num[0] -= 1
                            score += 100
                        elif num == 2:
                            enemies.remove(enemy)
                            missiles.remove(missile)
                            drop_items(health_item,enemy[0],enemy[1],1)
                            #enemies1_num[stage] -= 1
                            stage3_num[1] -= 1
                            score += 200
                        elif num == 3:
                            enemies.remove(enemy)
                            missiles.remove(missile)
                            drop_items(health_item,enemy[0],enemy[1],1)
                            #enemies1_num[stage] -= 1
                            stage3_num[2] -= 1
                            score += 300  # スコアを更新
                            #000点ごとに敵の速度を増加
                        if score % score_for_speed_increase == 0:
                            enemy_speed += 1
                            # stage += 1
                        if stage3_num[2] == 0:
                                drop_items(level_up_item,enemy[0],enemy[1],2)

                     
                except ValueError:
                    pass
                break

            if (missile[0] >= enemy[0] and missile[0] < enemy[0] + boss_size_x) and \
               (missile[1] >= enemy[1] and missile[1] < enemy[1] + boss_size_y):
                try:                  
                        if stage == 3:
                            if num == 4:
                                #enemies.remove(enemy)
                                missiles.remove(missile)
                                boss_hp -= 1
                                #enemies1_num[stage] -= 1
                                if boss_hp <= 0:
                                    enemies.remove(enemy)
                                    #drop_items(health_item,enemy[0],enemy[1])
                                    score += 10000
                except ValueError:
                    pass
                break

def collision_check_items(items,player_pos):
    global score, enemy_speed, score_for_speed_increase,stage,stage1_num,stage2_num,stage3_num,stage,boss_hp,health_item,player_health
    for item in items[:]:
        if (player_pos[0] >= item[0] and player_pos[0] < item[0] + enemy_size) and \
        (player_pos[1] >= item[1] and player_pos[1] < item[1] + enemy_size):
            try:
                    item.remove(item)
                    player_health += 1
                    #enemies1_num[stage] -= 1

            except ValueError:
                pass
            break


# テキストの描画
def draw_centered_text(text, font, color, y):
    text_surface = font.render(text, True, color)
    text_rect = text_surface.get_rect(center=(screen_width / 2, y))
    screen.blit(text_surface, text_rect)

def stage_up():
    global stage1_num,stage2_num,stage3_num,stage
    if stage == 0:
        if stage1_num[0] <= 0 and stage1_num[1] <= 0 and stage1_num[2] <= 0:
            stage += 1
            print("stage_up")
    elif stage == 1:
         if stage2_num[0] <= 0 and stage2_num[1] <= 0 and stage2_num[2] <= 0:
            stage += 1
            print("stage_up")
    elif stage == 2:
         if stage3_num[0] <= 0 and stage2_num[1] <= 0 and stage2_num[2] <= 0:
            stage += 1
            print("stage_up")
    elif stage == 3:
         if score >= 20000:
            stage += 1
            print("game_claer")

# スタート画面
def wait_for_start():
    global bg_y
    waiting = True
    while waiting:
        screen.fill(BLACK)
        draw_centered_text('Skull Blast', title_font, WHITE, screen_height / 3)
        draw_centered_text('Press Enter to Start', start_font, WHITE, screen_height / 2)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    waiting = False

# ゲーム開始前の待機状態
wait_for_start()

# ゲーム画面
def game_loop():
    global game_over, player_pos, enemies1,enemies2,enemies3, missiles,stage,boss,health_item,player_health,level_up_item,bg_y,time_sta,time_end

    time_sta = time.time()

    player_pos = [screen_width // 2, screen_height - player_size]
    enemies1 = []
    enemies2 = []
    enemies3 = []
    boss = []
    missiles = []
    game_over = False

    # ゲームオーバーになるまでループ
    while not game_over: 
        

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    if player_level == 0:
                        fire_missile(player_pos[0] + player_size // 2, player_pos[1])
                    elif player_level == 1:
                        fire_missile(player_pos[0] + player_size // 2 + 10, player_pos[1])
                        fire_missile(player_pos[0] + player_size // 2 - 10, player_pos[1])
                        # fire_missile(player_pos[0] + player_size // 2 - 10, player_pos[1])
                    elif player_level == 2:
                        fire_missile(player_pos[0] + player_size // 2, player_pos[1])
                        fire_missile(player_pos[0] + player_size // 2 + 20, player_pos[1])
                        fire_missile(player_pos[0] + player_size // 2 - 20, player_pos[1])
                if event.key == pygame.K_ESCAPE:  # ESCキーでゲーム終了
                    pygame.quit()
                    exit()

        keys = pygame.key.get_pressed()

        # プレイヤーの移動制御
        if keys[pygame.K_LEFT] and player_pos[0] > player_speed:
            player_pos[0] -= player_speed
        if keys[pygame.K_RIGHT] and player_pos[0] < screen_width - player_size - player_speed:
            player_pos[0] += player_speed

        # 画面の更新
        screen.fill(BLACK)

        stage_up()
        if stage == 4:
            break
        # 敵の生成と移動
        #if stage == 0:
        bg_y = (bg_y+2)%480
        screen.blit(img_bg,[0,bg_y-480])
        screen.blit(img_bg,[0,bg_y])
        if stage == 0:
            drop_enemies(enemies1,1)
            update_enemy_positions(enemies1, player_pos,1)
  
            update_item_positions(health_item, player_pos,1)
            update_item_positions(level_up_item, player_pos,2)
            # ミサイルの移動
            move_missiles(missiles)

            # 衝突判定
            collision_check(enemies1, missiles,1)

            # プレイヤーの描画
            draw_player()
            # 敵とミサイルの描画
            draw_enemies(enemies1,1)
            draw_missiles(missiles)
            draw_item(health_item,1)
            draw_item(level_up_item,2)
            

        if stage == 1:
            drop_enemies(enemies1,1)
            drop_enemies(enemies2,2)

            update_enemy_positions(enemies1, player_pos,1)

            update_enemy_positions(enemies2, player_pos,2)
        
            update_item_positions(health_item, player_pos,1)
            update_item_positions(level_up_item, player_pos,2)
            # ミサイルの移動
            move_missiles(missiles)

            # 衝突判定
            collision_check(enemies1, missiles,1)
            collision_check(enemies2, missiles,2)

            # プレイヤーの描画
            draw_player()
            # 敵とミサイルの描画
            draw_enemies(enemies1,1)
            draw_enemies(enemies2,2)
            draw_missiles(missiles)
            draw_item(health_item,1)
            draw_item(level_up_item,2)
        
        if stage == 2:
            drop_enemies(enemies1,1)
            drop_enemies(enemies2,2)
            drop_enemies(enemies3,3)

            update_enemy_positions(enemies1, player_pos,1)

            update_enemy_positions(enemies2, player_pos,2)
      
            update_enemy_positions(enemies3, player_pos,3)
   
            update_item_positions(health_item, player_pos,1)
            update_item_positions(level_up_item, player_pos,2)

            # ミサイルの移動
            move_missiles(missiles)

            # 衝突判定

            collision_check(enemies1, missiles,1)
            collision_check(enemies2, missiles,2)
            collision_check(enemies3, missiles,3)

            # プレイヤーの描画
            draw_player()
            # 敵とミサイルの描画
            draw_enemies(enemies1,1)
            draw_enemies(enemies2,2)
            draw_enemies(enemies3,3)
            draw_missiles(missiles)
            draw_item(health_item,1)
            draw_item(level_up_item,2)

        else :
            drop_enemies(boss,4)
            update_enemy_positions(boss, player_pos,4)
            update_item_positions(health_item, player_pos,1)
            update_item_positions(level_up_item, player_pos,2)
            update_item_positions(bomb, player_pos,3)
 
 
            

            # ミサイルの移動
            move_missiles(missiles)

            # 衝突判定
            collision_check(boss, missiles,4)
            #collision_check_items(health_item,player_pos)

            # プレイヤーの描画
            draw_player()
            # 敵とミサイルの描画
            draw_enemies(boss,4)
            draw_missiles(missiles)
            draw_item(health_item,1)
            draw_item(level_up_item,2)
            draw_item(bomb,3)

        # スコアの描画
        draw_score()
        #draw_stage()
        draw_health()
        if player_health <= 0:
            game_over = True
            break

        
        
        pygame.display.update()

        # ゲームの速度制御
        clock.tick(60)

    # ゲームオーバー時の処理
    if stage == 4:
        time_end = time.time()
        tim = time_end- time_sta
        tim = round(tim,1)
        screen.fill(BLACK)
        draw_centered_text('CONGRATULATIONS!!', game_over_font, WHITE, screen_height / 2 - 50)
        draw_centered_text('GAME CLELR', game_over_font, WHITE, screen_height / 2 - 100)
        draw_centered_text('Score: ' + str(score), score_font, WHITE, screen_height / 2 + 20)
        draw_centered_text('Time: ' + str(tim) + 's', score_font, WHITE, screen_height / 2 + 45)
        draw_centered_text('', score_font, WHITE, screen_height / 2 + 40)
        draw_centered_text('press enter to restart', game_over_restart_font, WHITE, screen_height / 2 + 70)
        draw_centered_text('or', game_over_restart_font, WHITE, screen_height / 2 + 100)
        draw_centered_text('esc to quit', game_over_restart_font, WHITE, screen_height / 2 + 130)
        pygame.display.update()

    if game_over :
        time_end = time.time()
        tim = time_end- time_sta
        tim = round(tim,1)
        screen.fill(BLACK)
        draw_centered_text('GAME OVER', game_over_font, WHITE, screen_height / 2 - 50)
        draw_centered_text('Score: ' + str(score), score_font, WHITE, screen_height / 2 + 20)
        draw_centered_text('Time: ' + str(tim) + 's', score_font, WHITE, screen_height / 2 + 45)
        draw_centered_text('', score_font, WHITE, screen_height / 2 + 40)
        draw_centered_text('press enter to restart', game_over_restart_font, WHITE, screen_height / 2 + 70)
        draw_centered_text('or', game_over_restart_font, WHITE, screen_height / 2 + 100)
        draw_centered_text('esc to quit', game_over_restart_font, WHITE, screen_height / 2 + 130)
        pygame.display.update()

    return wait_for_restart()

# リスタートまで待機
def wait_for_restart():
    global enemy_speed
    waiting = True
    while waiting:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    waiting = False
                    enemy_speed = 1
                    return True  # 再スタート
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    exit()

# ゲーム開始前の待機状態
wait_for_start()

# ゲームループ
restart = True
while restart:
    game_over = False
    score = 0  # スコアをリセット
    player_pos = [screen_width // 2, screen_height - player_size]
    enemies1 = []
    enemies2 = []
    enemies3 = []
    stage1_num = [20,0,0]
    stage2_num = [10,20,0]
    stage3_num = [10,10,30]
    health_item = []
    level_up_item = []
    bomb = []
    stage = 0
    boss_flag = 0
    missiles = []
    time_sta = 0
    restart = game_loop()

pygame.quit()

